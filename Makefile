# Set TARGET_SSH=user@host.local to decide which machine to run on.
TARGET_SSH:=pi@cottagepi.local

.PHONY: install
install:
	git push $(TARGET_SSH):src/qbutton-server HEAD:incoming

	ssh $(TARGET_SSH) "cd src/qbutton-server && git merge --ff-only incoming && git show -q && ~/.cargo/bin/cargo build"

	ssh $(TARGET_SSH) sudo cp src/qbutton-server/qbutton-server.service /etc/systemd/system/qbutton-server.service
	ssh $(TARGET_SSH) sudo systemctl daemon-reload
	ssh $(TARGET_SSH) sudo systemctl enable qbutton-server.service
	ssh $(TARGET_SSH) sudo systemctl restart qbutton-server.service
	ssh $(TARGET_SSH) sudo journalctl -u qbutton-server.service --output=cat --follow

.PHONY: setup
setup:
	ssh $(TARGET_SSH) "mkdir -p src"
	ssh $(TARGET_SSH) "[ -d src/qbutton-server ] || git clone https://gitlab.com/alsuren/qbutton-server src/qbutton-server"
	scp .env $(TARGET_SSH):src/qbutton-server/.env
