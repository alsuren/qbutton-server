use axum::{
    body::Body,
    http::{Response, StatusCode},
    response::IntoResponse,
};
use std::error::Error;

#[derive(Debug, Eq, PartialEq)]
pub struct InternalError {
    message: String,
}

impl From<Box<dyn Error>> for InternalError {
    fn from(e: Box<dyn Error>) -> Self {
        Self {
            message: format!("{}: {:?}", e, e),
        }
    }
}

impl IntoResponse for InternalError {
    fn into_response(self) -> Response<Body> {
        (StatusCode::INTERNAL_SERVER_ERROR, self.message).into_response()
    }
}
