use crate::config::Config;
use googapis::{
    google::assistant::embedded::v1alpha2::{
        assist_config, assist_request, audio_out_config::Encoding,
        embedded_assistant_client::EmbeddedAssistantClient, AssistConfig, AssistRequest,
        AudioOutConfig, DeviceConfig,
    },
    CERTIFICATES,
};
use oauth2::reqwest::async_http_client;
use oauth2::{AuthUrl, ClientId, ClientSecret, RefreshToken, TokenResponse, TokenUrl};
use tonic::{
    metadata::MetadataValue,
    transport::{Certificate, Channel, ClientTlsConfig},
    Request,
};

pub async fn get_token(config: &Config) -> Result<String, Box<dyn std::error::Error>> {
    let client = oauth2::basic::BasicClient::new(
        ClientId::new(config.client_id.to_string()),
        Some(ClientSecret::new(config.client_secret.to_string())),
        AuthUrl::new("https://oauth2.googleapis.com/auth".to_string())?,
        Some(TokenUrl::new(
            "https://oauth2.googleapis.com/token".to_string(),
        )?),
    );

    let token_response = client
        .exchange_refresh_token(&RefreshToken::new(config.refresh_token.to_string()))
        .request_async(async_http_client)
        .await?;

    Ok(token_response.access_token().secret().clone())
}

pub async fn make_request(
    bearer: String,
    command: String,
) -> Result<(), Box<dyn std::error::Error>> {
    let tls_config = ClientTlsConfig::new()
        .ca_certificate(Certificate::from_pem(CERTIFICATES))
        .domain_name("embeddedassistant.googleapis.com");

    let channel = Channel::from_static("https://embeddedassistant.googleapis.com")
        .tls_config(tls_config)?
        .connect()
        .await?;

    let mut service =
        EmbeddedAssistantClient::with_interceptor(channel, move |mut req: Request<()>| {
            let meta = MetadataValue::from_str(&format!("Bearer {}", bearer)).unwrap();
            req.metadata_mut().insert("authorization", meta);
            Ok(req)
        });

    let config = AssistConfig {
        r#type: Some(assist_config::Type::TextQuery(command)),
        // ..Default::Default() maybe?
        audio_out_config: Some(AudioOutConfig {
            encoding: Encoding::Mp3.into(),
            sample_rate_hertz: 16000,
            volume_percentage: 50,
        }),
        device_config: Some(DeviceConfig {
            device_id: "my_device_id".to_owned(),
            device_model_id: "qhome-887f8-qhome-button-ty2jrt".to_owned(),
        }),
        debug_config: None,
        dialog_state_in: None,
        screen_out_config: None,
    };
    let request = AssistRequest {
        r#type: Some(assist_request::Type::Config(config)),
    };
    let request = futures::stream::once(async move { request });
    let response = service.assist(request).await?;

    println!("RESPONSE={:?}", response);
    if let Some(msg) = response.into_inner().message().await? {
        eprintln!("MESSAGE={:?}", msg);
    }

    Ok(())
}
