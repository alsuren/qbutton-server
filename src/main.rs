mod assistant;
mod config;
mod error;

use crate::assistant::{get_token, make_request};
use crate::config::Config;
use crate::error::InternalError;
use axum::http::StatusCode;
use axum::{
    extract::{Path, State},
    response::{Html, IntoResponse},
    routing::get,
    Router,
};
use std::net::SocketAddr;
use tokio::net::TcpListener;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    run().await
}

async fn run() -> Result<(), Box<dyn std::error::Error>> {
    dotenv::dotenv().ok();
    let config = Config::from_env();

    // build our application with a route
    let app = Router::new()
        .route("/", get(root_handler))
        .route("/:command", get(command_handler).post(command_handler))
        .with_state(config);

    // run it
    let addr = SocketAddr::from(([0, 0, 0, 0], 8080));
    println!("listening on {}", addr);
    let listener = TcpListener::bind(&addr).await?;
    axum::serve(listener, app.into_make_service()).await?;

    Ok(())
}

fn command_link(url: &str) -> String {
    format!(r#"<a href="/{url}">/{url}</a>"#, url = url)
}

fn li(el: &str) -> String {
    format!("<li>{}</li>", el)
}

fn page(message: &str, config: &Config) -> Html<String> {
    Html(format!(
        r#"<html>
            <body>
                <p>{message}</p>
                <p>Available commands are: </p>
                <ul>{commands}</ul>
            </body>
        </html>"#,
        message = message,
        commands = config
            .allowed_commands
            .iter()
            .map(|c| li(&command_link(c)))
            .collect::<Vec<_>>()
            .join("\n")
    ))
}

async fn root_handler(State(config): State<Config>) -> Html<String> {
    page("Welcome to qbutton-server.", &config)
}

async fn command_handler(
    Path(command): Path<String>,
    State(config): State<Config>,
) -> Result<(StatusCode, Html<String>), InternalError> {
    // FIXME get axum to urldecode this for me?
    let command = dbg!(command.replace("%20", " "));

    if !config.allowed_commands.contains(&command) {
        return Ok((
            StatusCode::NOT_FOUND,
            page(
                &format!(
                    "command {:?} is not in ALLOWED_COMMANDS {:?}",
                    command, config.allowed_commands
                ),
                &config,
            ),
        ));
    }

    let token = get_token(&config).await?;
    make_request(token, command).await?;

    Ok((StatusCode::OK, page("Done", &config)))
}
