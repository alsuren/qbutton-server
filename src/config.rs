#[derive(Debug, Clone)]
pub struct Config {
    pub client_id: String,
    pub client_secret: String,
    pub refresh_token: String,
    pub allowed_commands: Vec<String>,
}

impl Config {
    pub fn from_env() -> Self {
        // FIXME: look into how axum passes context down into request handlers.

        let client_id =
            std::env::var("GOOGLE_CLIENT_ID").expect("GOOGLE_CLIENT_ID must be set in env or .env");
        let client_secret = std::env::var("GOOGLE_CLIENT_SECRET")
            .expect("GOOGLE_CLIENT_SECRET must be set in env or .env");
        let refresh_token =
            std::env::var("REFRESH_TOKEN").expect("REFRESH_TOKEN must be set in env or .env");

        let allowed_commands: Vec<String> = std::env::var("ALLOWED_COMMANDS")
            .expect("ALLOWED_COMMANDS must be set in env or .env")
            .split(',')
            .map(|s| s.to_string())
            .collect();
        Self {
            client_id,
            client_secret,
            refresh_token,
            allowed_commands,
        }
    }
}
